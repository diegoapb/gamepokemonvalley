# Generated by Django 2.0 on 2017-12-27 13:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Batalla',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('level', models.IntegerField(blank=True)),
                ('date_start', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Entrenador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(default='Jhon Doe', max_length=50)),
                ('nivel', models.IntegerField()),
                ('imgURL', models.TextField(blank=True, max_length=300, null=True)),
                ('fk_user', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Move',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('power', models.IntegerField(blank=True, null=True)),
                ('pp', models.IntegerField(blank=True, null=True)),
                ('pp_state', models.IntegerField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Pokemon',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_pokedex', models.IntegerField()),
                ('name', models.CharField(max_length=50)),
                ('hp', models.IntegerField()),
                ('hp_state', models.IntegerField()),
                ('attack', models.IntegerField()),
                ('defense', models.IntegerField()),
                ('front_shiny', models.URLField(max_length=300)),
                ('back_shiny', models.URLField(max_length=300)),
                ('fk_entrenador_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pokemon.Entrenador')),
                ('moves', models.ManyToManyField(blank=True, to='pokemon.Move')),
            ],
        ),
        migrations.CreateModel(
            name='Winner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('batalla', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pokemon.Batalla')),
                ('winner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pokemon.Entrenador')),
            ],
        ),
        migrations.AddField(
            model_name='batalla',
            name='entrenadores',
            field=models.ManyToManyField(blank=True, to='pokemon.Entrenador'),
        ),
    ]
