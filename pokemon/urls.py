from django.conf.urls import url, include
from pokemon import views
from rest_framework.routers import DefaultRouter


# Create a router and register our viewsets with it.

router = DefaultRouter()
router.register(r'entrenadores',views.EntrenadorViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'pokemon', views.PokemonViewSet)
router.register(r'moves', views.MoveViewSet)
router.register(r'batallass', views.BatallaViewSet)
router.register(r'winners', views.WinnerViewSet)
router.register(r'artitrainers',views.EntrenadorArtificialView,base_name="aritrainer")


# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^pruebas/$',views.pruebasRequest.as_view()),
    url(r'^cambiopoke/(?P<pk>[0-9]+)/$', views.cambiarPokemon.as_view()),
    url(r'^pokedex/',views.pokedexView.as_view()),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^auth/login/$', views.LoginView.as_view(), name='login'),
    url(r'^auth/logout/$', views.LogoutView.as_view(), name='logout'),


]