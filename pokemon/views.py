from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
# Serializer Class
from pokemon.serializers import UserSerializer, EntrenadorSerializer, PokemonSerializer, \
    WinnerSerializer, MoveSerializer, BatallaSerializer

# Views Class & decorators
from rest_framework import viewsets
# Para hacer las pruebas de la API request

from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import detail_route, list_route

# Import Models
from django.contrib.auth.models import User
from pokemon.models import Entrenador, Pokemon, Move, Winner, Batalla

#serializacion manual

from django.shortcuts import get_object_or_404
from django.utils.six import BytesIO
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

# import functions
import random

# functions to  consuming web API
from pokemon import services
import requests
import json
import pprint
from django.utils.six import BytesIO

# para hacer el login
from django.contrib.auth import authenticate,login,logout

from rest_framework import permissions

# Create your views here.
class pruebasRequest(APIView):

    def get (self,request,format=None):


        poke_info = services.get_info_pokemon(3)
        moves={}
        for j in range(0, 4):
            moves[j] = services.info_move(poke_info["moves"][j])
        return Response(poke_info)
    def post (self,request,format=None):

        return Response("POST peticion")

    def put(self, request, pk, format=None):
        pokemon = Pokemon.objects.get(pk=13)
        pokemon.__setattr__(self,"name","totiro")


class pokedexView(APIView):
    def get (self,request,format=None):
        cantidad_pokemon=151
        pokedex_info=services.pokedex_info(cantidad_pokemon)
        print(pokedex_info)
        for x in range(0,cantidad_pokemon):
            pokedex_info["results"][x]["front_shiny"]="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/"+ str(x+1) + ".png"
            pokedex_info["results"][x]["id"]=x+1
            pokedex_info["results"][x]["gif"]="http://www.pokestadium.com/sprites/xy/"+pokedex_info["results"][x]["name"]+".gif"
        return Response(pokedex_info)

class cambiarPokemon(APIView):

    def get_object(self, pk):
        try:
            return Pokemon.objects.get(pk=pk)
        except Pokemon.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        pokemon = Pokemon.objects.get(pk=pk)
        serializer_context = {
            'request': request,
        }
        serializer = PokemonSerializer(pokemon,context=serializer_context)
        return Response(serializer.data)


    def put(self, request, pk, format=None):
        pokemon = Pokemon.objects.get(pk=pk)
        Nrand=random.randint(1,151)
        poke_info=services.get_info_pokemon(Nrand)
        new_poke=new_pokemon(pokemon,poke_info,pokemon.fk_entrenador_id)
        new_poke.save()

        moves=pokemon.moves.all()


        for i in range(0,4):
            objMove = Move.objects.get(pk=moves[i].pk)

            info_mov=services.info_move(poke_info["moves"][i])
            move=new_move(objMove,info_mov)
            move.save()


        serializer_context = {
            'request': request,
        }
        serializer = PokemonSerializer(pokemon, context=serializer_context)

        return Response(serializer.data,status=status.HTTP_201_CREATED)


class EntrenadorArtificialView(viewsets.ModelViewSet):
    queryset = Entrenador.objects.all()
    serializer_class = EntrenadorSerializer



def create_pokemonRand(self,id,sr):
    print(sr.data)

    if sr.data.get("fk_user") is None:
        print("entro if")
        fk_entrenador_id=Entrenador.objects.get(id=sr.data.get("id"))
    else:
        fk_entrenador_id = Entrenador.objects.get(fk_user=self.request.user)
    print(fk_entrenador_id)


    poke_info = services.get_info_pokemon(id)

    pokemon= Pokemon.objects.create(fk_entrenador_id=fk_entrenador_id,
                                    id_pokedex=id,
                                    name=poke_info["name"],
                                    hp=poke_info["hp"],
                                    hp_state=poke_info["hp"],
                                    attack=poke_info["attack"],
                                    defense=poke_info["defense"],
                                    back_shiny=poke_info["sprites"]["back_shiny"],
                                    front_shiny=poke_info["sprites"]["front_shiny"])
    pokemon.save()
    for j in range(0,4):
        move_info=services.info_move(poke_info["moves"][j])
        move = Move.objects.create(name=move_info["name"],
                                   power=move_info["power"],
                                   pp=move_info["pp"],
                                   pp_state=move_info["pp"])
        move.save()
        pokemon.moves.add(move)

class LoginView(APIView):
    def get(self, request, format=None):
        return Response ("Estas en el GET De Login")
    def post(self, request, format=None):
        data= (request.data)
        username = data.get('username')
        password = data.get('password')
        account = authenticate(username=username, password=password)
        print(account)
        print("si paso")
        if account is not None:
            print("acount is not none")
            if account.is_active:
                print("acount is active")
                login(request, account)
                print("pasa login")
                print(request.data)
                serializer = UserSerializer(data=request.data)
                serializer.is_valid()
                return Response(serializer.data)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'This account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)

class LogoutView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        return Response ("Estas en el GET De Logout")

    def post(self, request, format=None):
        logout(request)

        return Response("Te esperamos pronto", status=status.HTTP_204_NO_CONTENT)


class UserViewSet (viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class EntrenadorViewSet (viewsets.ModelViewSet):
    queryset = Entrenador.objects.all()
    serializer_class = EntrenadorSerializer

    def get_queryset(self):
        if self.request.user.is_anonymous:
            return self.queryset.all()
        else:
            return self.queryset.filter(fk_user=self.request.user)

    def perform_create(self, serializer):
        '''
        si el usuario no tiene ningun entrenador registrado entonces crea un entrenador propio
        si el usuario ya tiene entrenador propio entonces se crea un entrenador artificial.
        el usuario podra crear varios entrenadores artificiales
        '''

        if (self.queryset.filter(fk_user=self.request.user).count()==0):
            serializer.save(fk_user=self.request.user)
        else:
            serializer.save(fk_user=None)

        print(serializer.data)

        for i in range(0, 6):
            if i == 5:
                id = 25
            else:
                id = random.randint(1, 151)
            create_pokemonRand(self,id,serializer)



def new_pokemon (pokemon,poke_info,entrenador):
    pokemon.fk_entrenador_id = entrenador
    pokemon.name = poke_info["name"]
    pokemon.hp = poke_info["hp"]
    pokemon.attack = poke_info["attack"]
    pokemon.defense = poke_info["defense"]
    pokemon.hp_state = poke_info["hp"]
    pokemon.front_shiny = poke_info["sprites"]["front_shiny"]
    pokemon.back_shiny = poke_info["sprites"]["back_shiny"]
    pokemon.id_pokedex = poke_info["id_pokedex"]
    return pokemon

def new_move(move, move_info):
    move.pp=move_info["pp"]
    move.name=move_info["name"]
    move.power=move_info["power"]
    move.pp_state=move_info["pp"]
    return move

class PokemonViewSet (viewsets.ModelViewSet):
    queryset = Pokemon.objects.all()
    serializer_class = PokemonSerializer

class MoveViewSet (viewsets.ModelViewSet):
    queryset = Move.objects.all()
    serializer_class = MoveSerializer


class WinnerViewSet (viewsets.ModelViewSet):
    queryset = Winner.objects.all()
    serializer_class = WinnerSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        entrenador = Entrenador.objects.get(fk_user=self.request.user)
        win10=self.queryset.filter(winner=entrenador).count()

        data = serializer.data
        print(win10)
        if 0==(win10%2):
            pokemons = Pokemon.objects.filter(fk_entrenador_id=entrenador)
            serializer_context = {
                'request': request,
            }
            pokemon_JSON = {}
            for i in range(0, 6):
                serializerP = PokemonSerializer(pokemons[i], context=serializer_context)
                pokemon_JSON[i] = {"urlpokemon":serializerP.data["url"],"urlchange":"http://localhost:8000/cambiopoke/"+str(pokemons[i].pk)}

            data["pokemonChangue"] = pokemon_JSON
        else:
            print("no changue")

        return Response(data, status=status.HTTP_201_CREATED, headers=headers)




class BatallaViewSet (viewsets.ModelViewSet):
    queryset = Batalla.objects.all()
    serializer_class = BatallaSerializer

