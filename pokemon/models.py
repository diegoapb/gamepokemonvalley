from django.db import models

# Create your models here.

class Entrenador (models.Model):
    nombre = models.CharField(max_length=50,default='Jhon Doe')
    nivel = models.IntegerField()
    imgURL = models.TextField(max_length=300,blank=True,null=True)
    fk_user = models.OneToOneField('auth.User',blank=True, null=True,on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {}'.format(self.nombre, self.fk_user)

class Batalla (models.Model):
    entrenadores = models.ManyToManyField(Entrenador,blank=True)
    level=models.IntegerField(blank=True)
    date_start=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        nombre_entrenador = ", ".join(str(seg) for seg in self.entrenadores.all())
        return '{}'.format(nombre_entrenador)

class Winner (models.Model):
    winner = models.ForeignKey(Entrenador,on_delete=models.CASCADE)
    batalla = models.OneToOneField(Batalla,on_delete=models.CASCADE)

    def __str__(self):
        return'{}{}'.format(self.winner.nombre,self.batalla.date_start)

class Move (models.Model):
    name = models.CharField(max_length=50)
    power = models.IntegerField(null=True,blank=True)
    pp = models.IntegerField(null=True,blank=True)
    pp_state = models.IntegerField(null=False,blank=True)

    def __str__(self):
        return '{}'.format(self.name)

class Pokemon (models.Model):
    fk_entrenador_id = models.ForeignKey(Entrenador,null=False,blank=False,on_delete=models.CASCADE)
    moves = models.ManyToManyField(Move,blank=True)
    id_pokedex = models.IntegerField(null=False,blank=False)
    name = models.CharField(max_length=50)
    hp =models.IntegerField(null=False,blank=False)
    hp_state = models.IntegerField(null=False)
    attack = models.IntegerField(null=False, blank=False)
    defense = models.IntegerField(null=False, blank=False)
    front_shiny = models.URLField(max_length=300)
    back_shiny = models.URLField(max_length=300)

    def __str__(self):
        return '{}'.format(self.name)


