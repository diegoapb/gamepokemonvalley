(function () {
	'use strict';

	angular.module('appPokemon').run(['$http', run]);

	function run($http) {
		$http.defaults.xsrfHeaderName = 'X-CSRFToken';
		$http.defaults.xsrfCookieName = 'csrftoken';
	}
})();