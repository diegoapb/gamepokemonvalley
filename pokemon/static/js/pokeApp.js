var pokeApp = angular.module('appPokemon', ['ui.router', 'ngCookies']);

pokeApp.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('login');

    $stateProvider
        .state('home',{
            url:'/home',
            templateUrl: 'static/html/home.html'
        })
        .state('login',{
            url:'/login',
            templateUrl: 'static/html/login.html',
            controller: 'ctrlLogin'
        })
        .state('register',{
            url:'/register',
            templateUrl: 'static/html/register.html',
            controller: 'ctrlRegister'
        })
        .state('battle',{
            url:'/battle',
            templateUrl: 'static/html/battle.html',
            controller: 'ctrlBattle'
        })
        .state('trainers',{
            url:'/trainers',
            templateUrl: 'static/html/trainers.html',
            controller: 'ctrlTrainers'
        })
        .state('create_trainers',{
            url:'/create_trainers',
            templateUrl: 'static/html/create_trainers.html',
            controller: 'ctrlCreateTrainers'
        })
        .state('profiletrainers',{
            url:'/profiletrainers',
            templateUrl: 'static/html/profiletrainers.html',
            controller: 'ctrlProfileTrainers',
            params:{
                id_user: null
            }

        })
        .state('battlevsia',{
            url:'/battlevsia',
            templateUrl: 'static/html/battlevsia.html',
            controller: 'ctrlBattleVsIa',
            params:{
                id_user: null
            }

        })
});


pokeApp.controller('ctrlLogin', function ($scope, $http, $state, $cookieStore) {
    var nickname = $cookieStore.get('nickname');
    var password = $cookieStore.get('password');

    if (!nickname){
       $scope.register = function () {
           $state.go('register');
       }

       $scope.login = function (nickname, password) {
           $http({
                    method: "POST",
                    url: "/api/api-auth/login/",                    
                    data: $.param({username: nickname, password: password}),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                    
                }).then(function mySuccess(response) {
                    if (response.status == 200){
                        $cookieStore.put('nickname', nickname);
                        $cookieStore.put('password', password);

                        /*$http({
                            method: "POST",
                            url: "/api/entrenadores/",                    
                            data: {
                                nombre : 'prueba',
                                imgURL: 'http://localhost:8000/prueba.img'
                                nivel: '1'
                            }
                        }).then(function mySuccess(response) {
                            if (response.status == 200){                                
                                console.log(response);
                            }else{
                                console.log(response);
                            }
                        }, function myError(response) {
                            $scope.myWelcome = response.statusText;
                        });
                        */

                        $state.go('create_trainers');                        
                        console.log(response);
                    }else{
                        console.log(response.statusText);
                        console.log(response.status);
                        console.log(response);
                    }
                }, function myError(response) {
                    $scope.myWelcome = response.statusText;
                });
       }
   }else {
        $state.go('battle');
    }
});


pokeApp.controller('ctrlRegister', function ($scope, $http, $state) {
   $scope.login = function () {
       $state.go('login');
   }

   $scope.register = function (email, nickname, password) {
       $http({
                method: "POST",
                url: "/api/users/",
                data: {
                    email: email,
                    username: nickname,
                    password: password
                }
            }).then(function mySuccess(response) {
                if (response.status == 201){
                    console.log(response)
                    $state.go('login');
                }else{
                    console.log(response.statusText);
                }

            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
   }
});


pokeApp.controller('ctrlBattle', function ($scope, $http, $state, $cookieStore) {
    var nickname = $cookieStore.get('nickname');
    var password = $cookieStore.get('password');
    //alert('hoal')
    //if (nickname){

         $http({
                method: "GET",
                url: "/api/entrenadores/",
                params: {
                    nickname: 'sebastian'
                }
            }).then(function mySuccess(response) {
                if (response.status == 200){


                    var pokemon = response.data[0].pokemon;

                    let m1 = pokemon.splice(0,(pokemon.length/2));
                     
                    let m2 = pokemon.splice(0,pokemon.length);
                    
                    $scope.datos = response.data[0];
                    $scope.pk1 = m1;
                    $scope.pk2 = m2;
                }else{
                    console.log(response.statusText);
                }
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
         });


         $scope.logout = function () {
             $cookieStore.remove('nickname');
             $cookieStore.remove('password');
             $http({
                method: "GET",
                url: "/api/api-auth/logout/",
                
            }).then(function mySuccess(response) {
                if (response.status == 200){

                }else{
                    console.log(response.statusText);
                }
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
             });

           $state.go('login');
        }

        $scope.trainers = function () {
            $state.go('trainers');
        }
    //}else {
    //    $state.go('login');
    //}

})


pokeApp.controller('ctrlTrainers', function ($scope, $http, $state, $cookieStore) {
    var nickname = $cookieStore.get('nickname');
    var password = $cookieStore.get('password');
    //alert('hoal')
    //if (nickname){

         $http({
                method: "GET",
                url: "/api/artitrainers/",
                params: {
                    nickname: 'sebastian'
                }
            }).then(function mySuccess(response) {
                if (response.status == 200){

                    console.log(response.data)

                    $scope.trainers = response.data;
                    
                }else{
                    console.log(response.statusText);
                }
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
         });


         $scope.logout = function () {
             $cookieStore.remove('nickname');
             $cookieStore.remove('password');
           $state.go('login');
        }

        $scope.battle = function () {
            $state.go('battle');
        }

        $scope.perfilTrainer = function (id) {
            $state.go('profiletrainers', {id_user: id})
        }

        $scope.startBattle = function (id) {
            $state.go('battlevsia', {id_user: id})
        }
    //}else {
    //    $state.go('login');
    //}

})

pokeApp.controller('ctrlProfileTrainers', function ($scope, $http, $state, $cookieStore, $stateParams) {
    var nickname = $cookieStore.get('nickname');
    var password = $cookieStore.get('password');
    //alert('hoal')
    //if (nickname){
    //

    id_user = $stateParams.id_user;

         $http({
                method: "GET",
                url: "/api/artitrainers/" + id_user,
                params: {
                    nickname: 'sebastian'
                }
            }).then(function mySuccess(response) {
                if (response.status == 200){


                    var pokemon = response.data.pokemon;

                    let m1 = pokemon.splice(0,(pokemon.length/2));
                     
                    let m2 = pokemon.splice(0,pokemon.length);
                    
                    $scope.datos = response.data;
                    $scope.pk1 = m1;
                    $scope.pk2 = m2;

                    //console.log(pokemon);
                    //console.log(m1);


                }else{
                    console.log(response.statusText);
                }
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
         });


         $scope.logout = function () {
             $cookieStore.remove('nickname');
             $cookieStore.remove('password');
           $state.go('login');
        }

        $scope.trainers = function () {
            $state.go('trainers');
        }

        $scope.battle = function () {
            $state.go('battle');
        }

    //}else {
    //    $state.go('login');
    //}

})


pokeApp.controller('ctrlCreateTrainers', function ($scope, $http, $state, $cookieStore) {
    var nickname = $cookieStore.get('nickname');
    var password = $cookieStore.get('password');
    //alert('hoal')
    //if (nickname){   
    
    $http({
                method: "GET",
                url: "/api/entrenadores/",
                params: {
                    nickname: 'sebastian'
                }
            }).then(function mySuccess(response) {
                if (response.status == 200){
                    console.log(response);
                    if (response.data.length > 0) {
                        $state.go('battle');
                    }
                    
                }else{
                    console.log(response.statusText);
                }
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
         });

    $scope.createTrainer = function (name, imgURL, nivel) {
        $("#submit").prop("disabled", true);

        $http({
                method: "POST",
                url: "/api/entrenadores/",
                data: {
                    nombre: name,
                    imgURL: imgURL,
                    nivel: '1'
                }
            }).then(function mySuccess(response) {
                if (response.status == 201){
                    
                    $state.go('battle');

                    $scope.trainers = response.data;
                    
                }else{
                    console.log(response.statusText);
                }
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
         });

        //$("#submit").attr("disabled", true);
        
    }

    $scope.logout = function () {
         $cookieStore.remove('nickname');
         $cookieStore.remove('password');
       $state.go('login');
    }

    $scope.trainers = function () {
        $state.go('trainers');
    }

})




pokeApp.controller('ctrlBattleVsIa', function ($scope, $http, $state, $cookieStore, $stateParams) {
    var nickname = $cookieStore.get('nickname');
    var password = $cookieStore.get('password');
    //alert('hoal')
    //if (nickname){
    //
    var entrenador, ia;

    id_user = $stateParams.id_user;

    var ejecutar = function() {
       $http({
                method: "GET",
                url: "/api/artitrainers/" + id_user,
                params: {
                    nickname: 'sebastian'
                },
                async: false                
            }).then(function mySuccess(response) {
                if (response.status == 200){

                    console.log(response.data)
                    ia = response.data;
                }else{
                    console.log(response.statusText);
                }
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
         });

        $http({
                method: "GET",
                url: "/api/entrenadores/",
                params: {
                    nickname: 'sebastian'
                },
                async: false               
            }).then(function mySuccess(response) {
                if (response.status == 200){

                    console.log(response.data[0]);
                    entrenador = response.data[0];
                }else{
                    console.log(response.statusText);
                }
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
         });
            return true;
    }

    var logic = function() {
        var iaJson = ia;
        var entrenadorJson = entrenador;
        var entrenadorIMG, trainerNamePokemon, hpTrainerPk, attackTrainerPk, iaNamePokemon, iaIMG, hpIaPk, attackIaPk, pokemoniaDOM, pkNameIa, pokemonEntrenadorDOM, pkNameTrainer, hpTrainerDOM, hpTrainerActualDOM, hpTrainerDef, hpIaDef ; 


        var posicionx, posiciony;

        posicionx = 0;
        posiciony = 5;



        function pokemons(x,y){
            entrenadorIMG = entrenadorJson.pokemon[x].back_shiny;
            trainerNamePokemon = entrenadorJson.pokemon[x].name;
            hpTrainerPk = entrenadorJson.pokemon[x].hp + 100;
            hpTrainerDef = entrenadorJson.pokemon[x].hp + 100;    
            attackTrainerPk = entrenadorJson.pokemon[x].attack;
                
            
            iaNamePokemon = iaJson.pokemon[y].name;
            iaIMG = iaJson.pokemon[y].front_shiny;  
            hpIaPk = iaJson.pokemon[y].hp + 100;
            hpIaDef = iaJson.pokemon[y].hp + 100;
            attackIaPk = iaJson.pokemon[y].attack;
        }

        pokemons(0,5);
        /*IA*/
        pokemoniaDOM = document.querySelector('#pokemon_img_ia');
        pokemoniaDOM.src = iaIMG

        pkNameIa = document.querySelector('#name_pk_ia');
        pkNameIa.textContent = iaNamePokemon;
        /**/


        /*TRAINER*/
        pokemonEntrenadorDOM = document.querySelector('#pokemon_img_trainer');
        pokemonEntrenadorDOM.src = entrenadorIMG;

        pkNameTrainer = document.querySelector('#name_pk_trainer');
        pkNameTrainer.textContent = trainerNamePokemon;

        hpTrainerDOM = document.querySelector('#fullHpTrainer');
        hpTrainerDOM.textContent = hpTrainerPk

        hpTrainerActualDOM = document.querySelector('#hpTrainer');
        hpTrainerActualDOM.textContent = hpTrainerPk;
        /**/

         console.log(hpTrainerPk + ' Vida del entrenador');
        console.log(hpIaPk +' Vida de la ia');    



        function selectImg(x) {
            x.classList.add("arrow")    
        }

        function normalImg(x) {
            x.classList.remove("arrow");
        }
        function atacar(){      
           
            hpIaPk = hpIaPk - attackTrainerPk;
            console.log( trainerNamePokemon + " ataca a " + iaNamePokemon + " con " + attackTrainerPk + " de daño");
            hpTrainerPk = hpTrainerPk - attackIaPk;
            console.log( iaNamePokemon + " ataca a " + trainerNamePokemon + " con " + attackIaPk + " de daño");
            console.log(hpTrainerPk + ' Vida del entrenador actual');
            console.log(hpIaPk +' Vida de la ia actual');    
            
            hpTrainerActualDOM.textContent = hpTrainerPk;
            downLife(hpTrainerPk, hpIaPk);
            
            if(hpIaPk <= 0){
                console.log('has ganado papu');        
                posiciony = posiciony - 1;
                if(posiciony < 0){
                    console.log('has ganado definitivamente');        

                }else{
                    cambiarPokemon('ia', posicionx);
                }
                
            }else if(hpTrainerPk <= 0){
              console.log('ha ganado la ia');
                posicionx = posicionx + 1;
                if(posicionx > 5){
                    console.log('has perdido papu definitivamente');  
                    
                }else{            
                    cambiarPokemon('trainer', posicionx);
                 }
            }
        }

        function cambiarPokemon(ganador, pos){
            if(ganador === 'trainer'){
                entrenadorIMG = entrenadorJson.pokemon[pos].back_shiny;
                trainerNamePokemon = entrenadorJson.pokemon[pos].name;
                hpTrainerPk = entrenadorJson.pokemon[pos].hp + 100;
                attackTrainerPk = entrenadorJson.pokemon[pos].attack;
                hpTrainerDef = entrenadorJson.pokemon[pos].hp + 100;  
                console.log('ENTROOOOOO');
                
                pokemonEntrenadorDOM = document.querySelector('#pokemon_img_trainer');
                pokemonEntrenadorDOM.src = entrenadorIMG;

                pkNameTrainer = document.querySelector('#name_pk_trainer');
                pkNameTrainer.textContent = trainerNamePokemon;

                hpTrainerDOM = document.querySelector('#fullHpTrainer');
                hpTrainerDOM.textContent = hpTrainerPk

                hpTrainerActualDOM = document.querySelector('#hpTrainer');
                hpTrainerActualDOM.textContent = hpTrainerPk;
                
                
                
            }else if(ganador === 'ia'){
                iaNamePokemon = iaJson.pokemon[pos].name;
                iaIMG = iaJson.pokemon[pos].front_shiny;  
                hpIaPk = iaJson.pokemon[pos].hp + 100;
                attackIaPk = iaJson.pokemon[pos].attack;
                hpIaDef = iaJson.pokemon[pos].hp + 100;
                
                pokemoniaDOM = document.querySelector('#pokemon_img_ia');
                pokemoniaDOM.src = iaIMG
                pkNameIa = document.querySelector('#name_pk_ia');
                pkNameIa.textContent = iaNamePokemon;      
                
                
            }
        }

        var downLife = function(trainer, rival){
            
            
            trainer = (209 * trainer) / hpTrainerDef;
            rival = (209 * rival) / hpIaDef;
            console.log(trainer + 'trainerrrrrrrrrrrr');
            console.log(rival + 'rivallllllllllllll');                
            
            
            $("#lRival").animate({width: rival},"slow",function(){
                $("#lTrainer").animate({width: trainer},"slow",function(){
                    console.log(parseInt($("#lTrainer").css("width")) <= 0);
                    if (parseInt($("#lTrainer").css("width")) <= 0){
                        $("#lTrainer").css({"width": "255px"});
                    }

                    if(parseInt($("#lRival").css("width")) <= 0){
                        $("#lRival").css({"width": "255px"});
                    }
                });
            });
        } 


        $(".m").hover(function(){
            $(this).addClass("arrow");
            }, function(){
            $(this).removeClass("arrow");
        });

        $(".m").click(function(){
            atacar();
        });

    }

var myCallback = function(funcion1, funcion2){
    if(funcion1()){
        setTimeout(function(){ funcion2(); }, 1000);
    }        

}
    
    myCallback(ejecutar,logic);
     



         $scope.logout = function () {
             $cookieStore.remove('nickname');
             $cookieStore.remove('password');
           $state.go('login');
        }

        $scope.trainers = function () {
            $state.go('trainers');
        }
    //}else {
    //    $state.go('login');
    //}

})