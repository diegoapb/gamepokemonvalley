import requests
import random
import json

def get_info_pokemon (id):
    url = 'https://pokeapi.co/api/v2/pokemon/'+str(id)+'/'
    r = requests.get(url)
    json_data = r.json()

    #dicccionario que contiene los datos tomados de la API
    pokemon_info = {}
    #toma el nombre del pokemon y lo almacen en el dict
    pokemon_info["name"]=json_data.get('name')
    #almacena los valores de hp, defensa y ataque de un poqumon
    stats = json_data.get("stats")
    pokemon_info["hp"]=stats[5]["base_stat"]
    pokemon_info["attack"] = stats[4]["base_stat"]
    pokemon_info["defense"] = stats[3]["base_stat"]
    #almacena las url de los 4 movimientos que va a tener el pokemon
    moves={}
    data_moves = json_data.get("moves")

    top_rand = len(data_moves)
    print(top_rand)
    for x in range (0,4):
        rand=random.randint(0,len(data_moves)-1)
        print(rand)
        moves[x]=data_moves[rand]["move"]["url"]
    pokemon_info["moves"]=moves
    #almacena las url de la parte frontal y trasera de un pokemon
    pokemon_info["sprites"]={
        "front_shiny":json_data.get("sprites")["front_shiny"]
        ,"back_shiny":json_data.get("sprites")["back_shiny"]}
    pokemon_info["id_pokedex"]=json_data.get("id")
    return pokemon_info

def info_move (url):
    r = requests.get(url)
    data=r.json()
    move={}
    move["name"]=data.get("name")
    move["power"]=data.get("power")
    move["pp"]=data.get("pp")
    return move

def pokedex_info(cantidad):
    r = requests.get("https://pokeapi.co/api/v2/pokemon/?limit="+str(cantidad)+"&offset=0")
    data = r.json()
    return data