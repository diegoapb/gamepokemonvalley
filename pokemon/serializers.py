
from django.contrib.auth.models import User
from rest_framework import serializers
# Importar modelos
from pokemon.models import Entrenador,Pokemon,Move,Batalla,Winner

class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True)
    #entrenador = serializers.ReadOnlyField(read_only=True)
    class Meta:
        model = User
        fields = ('url','id', 'email', 'password','username')

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

class MoveSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Move
        fields = ('url','name','power','pp','pp_state')

class PokemonSerializer(serializers.HyperlinkedModelSerializer):
    moves = MoveSerializer(many=True)
    class Meta:
        model = Pokemon
        fields ="__all__"



class BatallaSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Batalla
        fields = "__all__"

class WinnerSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Winner
        fields = "__all__"

class EntrenadorSerializer(serializers.HyperlinkedModelSerializer):

    fk_user = UserSerializer(read_only=True)
    pokemon = PokemonSerializer(many=True, read_only=True, source='pokemon_set')

    class Meta:
        model = Entrenador
        fields = ('url',"id",'nombre','imgURL','nivel','pokemon','fk_user')



