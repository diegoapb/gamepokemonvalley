from django.contrib import admin
from pokemon import models
# Register your models here.

admin.site.register(models.Entrenador)
admin.site.register(models.Move)
admin.site.register(models.Pokemon)
admin.site.register(models.Batalla)
admin.site.register(models.Winner)