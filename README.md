# Pokemon Valley

Pokemon Valley es un juego web, en el cual podras enfrentarte a otros entrenadores y poner a prueba tus pokemon
Pokemon Valley funciona como un API Rest y usa los servicios de la PokeAPI

## Getting Started

Para abrir el proyecto debes:

1. git clone ...
2. pip install -r requeriments.txt
3. docker start pgdb                (despues de haber instalado el repositorio)
4. python manage.py migrate
5. python manage.py runserver


### Prerequisites

Debes tener instalados:

```
docker
python3
django
virtualenv (recomendado)
```

### Installing

crear un repositorio de docker con postgres

```
$ docker run --name pgdb -e POSTGRES_USER=postgres -e POSTGRES_PASWORD=abcd1234 -d -p 3000:5432 postgres
```

ingresar al repositorio para poder acceder a postgresql

```
$ docker exec -it pgdb bash
```

ingresar a postgres usando el usuario, el host y el puerto

```
:/# psql -U postgres -h 127.17.0.2 -p 5432
```
Crear base de datos pk:

```
=# create DATABASE pk
=# \q
```

Salir de docker (exit)

```
/# exit
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc